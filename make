#!/bin/bash

function nasm_compile
{
    nasm -I ./source/ -felf64 ./source/$1.asm -o ./temp/$1.o
}


nasm_compile main
nasm_compile thread
nasm_compile queue
nasm_compile msg
gcc -std=gnu11  -c ./source/test.c -o ./temp/test.o
gcc -g ./temp/*.o
