/*
;;;
;;;   tmsgx64
;;;
;;;   Copyright 2013 Tetsumi <tetsumi@vmail.me>
;;; 
;;;   This program is free software: you can redistribute it and/or modify
;;;   it under the terms of the GNU General Public License as published by
;;;   the Free Software Foundation, either version 3 of the License, or
;;;   (at your option) any later version.
;;;
;;;   This program is distributed in the hope that it will be useful,
;;;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;   GNU General Public License for more details.
;;;
;;;   You should have received a copy of the GNU General Public License
;;;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;

*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

typedef void* Queue;

uint64_t msg_consumer  (Queue q);
void*    queue_create  (uint32_t length);
uint64_t queue_destroy (Queue q);
uint64_t queue_enqueue (Queue q, void *element);
void    *queue_dequeue (Queue q);
uint64_t thread_create (uint64_t (*f)(), void *argument);
void     thread_exit   (uint64_t code);
uint64_t thread_yield  (void);
void     sync_printf   (char *str, ...);

typedef struct
{
	Queue    queue;
	void	*sender;
	void	*receiver;
	int32_t	 length;
	char	 string[64];
	int	 a;
	int	 b;
	int	 c;
} Message_A;

typedef struct
{
	void (*msg_handler)(Message_A *m);
} Object;

Message_A gPing;
Message_A gPong;

void print_msg (Message_A *m)
{
	sync_printf("Object %p received message \"%s\" from Object %p "
		    "(Arguments a: %u b: %u c: %u)\n",
		    m->receiver,
		    m->string,
		    m->sender,
		    m->a,
		    m->b,
		    m->c);
}

void msg_handler_pong (Message_A *m)
{
	print_msg(m);	
	gPing.a = rand();
	gPing.b = rand();
	++gPing.c;
	queue_enqueue(m->queue, &gPing);
}

void msg_handler_ping (Message_A *m)
{
	print_msg(m);
	if (m->c > 2000)
		return;
	gPong.a = m->a;
	gPong.b = m->b;
	gPong.c = m->c;
	queue_enqueue(m->queue, &gPong);
}


Object gOa = { .msg_handler = msg_handler_pong };
Object gOb = { .msg_handler = msg_handler_ping };

int ctest (void)
{
	puts(__FUNCTION__);
	
	srand(0xfa48e4);
	
	Queue queue = queue_create(128);
   
	if (!queue)
		return 1;
			
	gPing = (Message_A){
		.queue = queue,
		.sender = &gOa,
		.receiver = &gOb,
		.length = 64,
		.string = "ping",
		.a = 123,
		.b = 321,
		.c = 1
	};

	gPong = (Message_A){
		.queue = queue,
		.sender = &gOb,
		.receiver = &gOa,
		.length = 64,
		.string = "pong",
		.a = 789,
		.b = 987,
		.c = 798
	};
	
	queue_enqueue(queue, &gPing);
	thread_create(msg_consumer, queue);
	
	return 0;
}
