;;;
;;;   tmsgx64
;;;
;;;   Copyright 2013 Tetsumi <tetsumi@vmail.me>
;;; 
;;;   This program is free software: you can redistribute it and/or modify
;;;   it under the terms of the GNU General Public License as published by
;;;   the Free Software Foundation, either version 3 of the License, or
;;;   (at your option) any later version.
;;;
;;;   This program is distributed in the hope that it will be useful,
;;;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;   GNU General Public License for more details.
;;;
;;;   You should have received a copy of the GNU General Public License
;;;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;
	; Exports
global msg_consumer

	; Imports
extern queue_dequeue
	
;;; Message header
;;;      queue's address : U64
;;;     sender's address : U64
;;;   receiver's address : U64
;;;      string's length : U32
;;;               string : U8[any]
;;;            arguments : U8[any]
	
section .text
	
;;; msg_consumer
;;;     rdi: messages queue's address
;;; return
;;;     rax: 0 if success
msg_consumer:
	push r13
	; Verify argument
	test rdi, rdi
	jz .error
	mov r13, rdi
.loop:
	; Dequeue next message
	mov rdi, r13
	call queue_dequeue
	cmp rax, 0
	je .loop
	mov rdi, rax
	
	; Get receiver's address
	mov rsi, [rax + 16]

	; Call receiver's message handler
	call [rsi]
	jmp .loop
	
	pop r13
	xor rax, rax
	ret
.error:
	pop r13
	mov rax, 1
	ret
