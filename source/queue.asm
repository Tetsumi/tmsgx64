;;;
;;;   tmsgx64
;;;
;;;   Copyright 2013 Tetsumi <tetsumi@vmail.me>
;;; 
;;;   This program is free software: you can redistribute it and/or modify
;;;   it under the terms of the GNU General Public License as published by
;;;   the Free Software Foundation, either version 3 of the License, or
;;;   (at your option) any later version.
;;;
;;;   This program is distributed in the hope that it will be useful,
;;;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;   GNU General Public License for more details.
;;;
;;;   You should have received a copy of the GNU General Public License
;;;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;
	
;;; Queue header
;;;            lock : U32
;;;          length : U32
;;;   enqueue index : U32
;;;   dequeue index : U32

	; Exports
	
global queue_create
global queue_enqueue
global queue_dequeue
global queue_destroy
	
	; Imports


	SYS_MMAP      	equ          9
	SYS_MUNMAP      equ         11
	PROT_READWRITE 	equ          3
	MAP_PRIVANON   	equ         34
	
;;; queue_create
;;;     rdi: length
;;; return
;;;     rax: queue's address
queue_create:
	; Check for null
	test edi, edi
	jz .error
	push rdi

	; Compute memory size
	lea rsi, [(rdi * 8) + 16]

	; Allocation
	mov rax, SYS_MMAP
	xor rdi, rdi
	mov rdx, PROT_READWRITE
	mov r10, MAP_PRIVANON
	xor r9, r9
	xor r8, r8
	syscall
	cmp rax, 0
	pop rdi
	jl .error
	
	; Store queue's length
	mov dword [rax + 4], edi
	ret
.error:
	mov rax, 0
	ret

;;; queue_destroy
;;;    rdi: queue's address
;;; return
;;;    rax: 0 if success
queue_destroy:
	; Verify argument
	test rdi, rdi
	jz .error
	; Compute memory length
	mov rsi, [rdi + 4]
	lea rsi, [(rsi * 8) + 16]
	mov rax, SYS_MUNMAP
	syscall
	cmp rax, 0
	jne .error
	xor rax, rax
	ret
.error:
	mov rax, 1
	ret

;;; queue_enqueue
;;;     rdi: queue's address
;;;     rsi: object's address
;;; return
;;;     rax: 0 if success.
queue_enqueue:
	; Verify arguments
	test rdi, rdi
	jz .error
	test rsi, rsi
	jz .error

	; Try lock
	mov rax, 0
	mov rcx, 1
	lock cmpxchg dword [rdi], ecx
	jnz .error

	; Check if current slot is free
	mov eax, [rdi + 8]
	lea rcx, [(rdi + 16) + (rax * 8)]
	mov rdx, [rcx]
	cmp rdx, 0
	jne .error_unlock

	; Set slot
	mov [rcx], rsi

	; Update enqueue index
	inc eax
	cmp [rdi + 4], eax
	jne .update_index
	xor eax, eax
.update_index:
	mov [rdi + 8], eax
	
	; Unlock
	mov dword [rdi], 0
	xor rax, rax
	ret
.error_unlock:
	; Unlock
	mov dword [rdi], 0
.error:
	mov rax, 1
	ret

;;; queue_dequeue
;;;     rdi: queue's address
;;; return
;;;     rax: object's address
queue_dequeue:
	; Verify argument
	test rdi, rdi
	jz .error

	; Try lock
	mov rax, 0
	mov rcx, 1
	lock cmpxchg dword [rdi], ecx
	jnz .error

	; Check if current slot is free
	mov edx, [rdi + 12]
	lea rcx, [(rdi + 16) + (rdx * 8)]
	mov rax, [rcx]
	cmp rax, 0
	je .unlock
	mov qword [rcx], 0
	
	; Update dequeue index
	inc edx
	cmp [rdi + 4], edx
	jne .update_index
	xor edx, edx
.update_index:
	mov [rdi + 12], edx
.unlock:
	mov dword [rdi], 0
	ret
.error:
	xor rax, rax
	ret
