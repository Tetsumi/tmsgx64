;;;
;;;   tmsgx64
;;;
;;;   Copyright 2013 Tetsumi <tetsumi@vmail.me>
;;; 
;;;   This program is free software: you can redistribute it and/or modify
;;;   it under the terms of the GNU General Public License as published by
;;;   the Free Software Foundation, either version 3 of the License, or
;;;   (at your option) any later version.
;;;
;;;   This program is distributed in the hope that it will be useful,
;;;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;   GNU General Public License for more details.
;;;
;;;   You should have received a copy of the GNU General Public License
;;;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;

	; Exports
global thread_create
global thread_exit
global thread_yield

	; Imports
extern printf


	STACK_SIZE 	equ    2097152
	SYS_CLONE 	equ         56
	SYS_MMAP      	equ          9
	SYS_MUNMAP      equ         11
	SYS_EXIT        equ         60
	SYS_ARCH_PRCTL  equ        158
	SYS_SCHED_YIELD equ         24
	PROT_READWRITE 	equ          3
	MAP_PRIVANON   	equ         34
	THREAD_FLAGS    equ 0x80018f00
	ARCH_SET_GS     equ     0x1001
	ARCH_SET_FS     equ     0x1002
	ARCH_GET_FS     equ     0x1003
	ARCH_GET_GS     equ     0x1004

section .rodata	
	strTest db `ret syscall %u\n\0`
	
section .text

;;; wrapper_function
;;; return
;;;     none
wrapper_function:
	mov rax, SYS_ARCH_PRCTL
	mov rdi, ARCH_SET_GS
	mov rsi, [rsp - 16]
	syscall
		
	; Set procedure's argument
	mov rdi, [rsp - 32]
	
	; Call the procedure
	call [rsp - 24]
	
	; Exit
	mov rdi, 0
	call thread_exit
	ret

;;; thread_exit
;;;     rdi: exit code
;;; return
;;;     none
thread_exit:
	mov r10, rdi
	mov rax, SYS_ARCH_PRCTL
	mov rdi, ARCH_GET_GS
	lea rsi, [rsp - 8]
	syscall
	
	; Free call stack's memory
	mov rax, SYS_MUNMAP
	mov rdi, [rsp - 8]
	mov rsi, STACK_SIZE
	syscall

	; Exit
	mov rax, SYS_EXIT
	mov rdi, r10
	syscall
	ret
		
;;; thread_create
;;; 	rdi: procedure's address
;;; 	rsi: procedure's argument
;;; return
;;; 	rax: thread's id
thread_create:
	; Check for null
	cmp rdi, 0
	je .error

	; Save arguments for later
	push rsi
	push rdi
	
	; Allocation of thread's call stack
	mov rax, SYS_MMAP
	xor rdi, rdi
	lea rsi, [STACK_SIZE + 16]
	mov rdx, PROT_READWRITE
	mov r10, MAP_PRIVANON
	xor r8, r8
	xor r9, r9
	syscall
	cmp rax, 0
	jl .error_pop
	mov r8, rax
	add r8, 16
	and r8, 0xFFFFFFFFFFFFFFF0
	
	; Set the return address to procedure's address
	lea rsi, [r8 + STACK_SIZE - 8]
	mov qword [rsi], wrapper_function

	; Save wrapper's arguments on thread's call stack
	mov [r8 + STACK_SIZE - 16], rax
	pop qword [r8 + STACK_SIZE - 24]
	pop qword [r8 + STACK_SIZE - 32]
	
	; Create the thread
	mov rax, SYS_CLONE
	mov rdi, THREAD_FLAGS
	xor rdx, rdx
	xor r10, r10
	syscall
	cmp rax, 0
	jl .error_clone
	ret
.error_clone:
	mov rax, SYS_MUNMAP
	mov rdi, rax
	mov rsi, STACK_SIZE
	syscall
.error_pop:
        pop rdi
        pop rsi
.error:
	mov rax, -1
	ret
	
;;; thread_yield
;;; return
;;;     rax: 0 if success
thread_yield:
	mov rax, SYS_SCHED_YIELD
	syscall
	ret
	
	
