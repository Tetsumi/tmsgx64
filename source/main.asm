;;;
;;;   tmsgx64
;;;
;;;   Copyright 2013 Tetsumi <tetsumi@vmail.me>
;;; 
;;;   This program is free software: you can redistribute it and/or modify
;;;   it under the terms of the GNU General Public License as published by
;;;   the Free Software Foundation, either version 3 of the License, or
;;;   (at your option) any later version.
;;;
;;;   This program is distributed in the hope that it will be useful,
;;;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;   GNU General Public License for more details.
;;;
;;;   You should have received a copy of the GNU General Public License
;;;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;

	;; Exports
global main
global sync_printf
global strRsp
	
	;; Imports
extern printf
extern thread_create
extern thread_yield
extern queue_create
extern queue_enqueue
extern queue_dequeue
extern ctest

	SYS_NANOSLEEP   equ         35
	SYS_ARCH_PRCTL  equ        158
	ARCH_SET_GS     equ     0x1001
	ARCH_SET_FS     equ     0x1002
	ARCH_GET_FS     equ     0x1003
	ARCH_GET_GS     equ     0x1004
	
section .rodata
	
	strMain     db `Main thread\n\0`
	strMainExit db `Main thread exit!\n\0`
	strThread   db `test_thread_proc(%u)\n\0`
	strEax      db `EAX = %u %d\n\0`
	strProducer db `Producer: %u\n\0`
	strConsumer db `Consumer: %u\n\0`
	
section .data
	printlock db 0
	tempint   dd 0
	tempreg   dq 0
timeval:
	tv_sec    dq 3
	tv_usec   dq 0
	
section .text

;;; Entry point
main:	
	push r13
	xor rax, rax
	mov rdi, strMain
	call printf

	mov rdi, 5
	call queue_create
	mov rsi, rax
	mov r13, rax
	mov rdx, rax
	xor rax, rax
	mov rdi, strEax
	call printf

	; Producer
	mov rdi, test_producer
	mov rsi, r13
	call thread_create

	; Consumer
	mov rdi, test_consumer
	mov rsi, r13
	call thread_create

	; Call C test procedure
	call ctest

	; sleep 5 seconds
	mov rax, SYS_NANOSLEEP
	mov rdi, timeval
	xor rsi, rsi
	syscall

	; Exit
	xor rax, rax
	mov rdi, strMainExit
	call sync_printf
	;call printf
	
	xor rax, rax
	pop r13
	ret

test_producer:
	sub rsp, 8
	push r13
	push r14
	mov r13, rdi
	mov r14, 1
.loop:
	mov rdi, r13
	mov rsi, r14
	call queue_enqueue
	cmp rax, 0
	jne .loop
	xor rax, rax
	mov rdi, strProducer
	mov rsi, r14
	call sync_printf
	inc r14
	cmp r14, 16
	jne .loop
	pop r14
	pop r13
	add rsp, 8
	ret

test_consumer:
	sub rsp, 8
	push r13
	push r14
	mov r13, rdi
	mov r14, 1
.loop:
	mov rdi, r13
	call queue_dequeue
	cmp rax, 0
	je .loop
	mov rsi, rax
	xor rax, rax
	mov rdi, strConsumer
	call sync_printf
	inc r14
	cmp r14, 16
	jne .loop
	pop r14
	pop r13
	add rsp, 8
	ret
	
sync_printf:
	push rax
	push rcx
.loop:
	mov rax, 0
	mov rcx, 1
	lock cmpxchg byte [printlock], cl
	jz .locked
	call thread_yield
	jmp .loop
.locked:
	pop rcx
	pop rax
	pop qword [tempreg]
	push qword .ret
	jmp printf
.ret:
	push qword [tempreg]
	mov byte [printlock], 0
	ret
